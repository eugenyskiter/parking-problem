package main

import (
	"fmt"
)

const (
	CarNumbers = 100_000_000 // Кол-во автомобилей, участвующих в эксперименте.
	HoursInDay = 24          // Кол-во часов в сутках.
)

type ParkingTime struct {
	Start uint8 // Время заезда автотранспорта на парковку.
	End   uint8 // Время выезда автотранспорта с парковки.
}

var (
	// ParkingTimes - последовательность случаев парковки
	ParkingTimes = make([]ParkingTime, 0, CarNumbers)

	// Experiment - хэш-мап, который содержит количество автомобилей на каждый час в сутках
	Experiment = make([]int, HoursInDay)
)

func main() {
	// Сбор статистики, по количеству автомобилей на каждый час
	for _, parkingTime := range ParkingTimes {
		for key, _ := range Experiment {
			idx := uint8(key)
			if InBetween(&idx, &parkingTime.Start, &parkingTime.End) {
				Experiment[key] += 1
			}
		}
	}
	PrintMemUsage("После расчета количества автомобилей на каждый час")

	CommonReport(&Experiment)

	idx, val := Max(&Experiment)
	fmt.Println("--------------")
	fmt.Printf("Наибольшее кол-во автомобилей (%d) было зафиксированно в %d часов", val, idx)
}
