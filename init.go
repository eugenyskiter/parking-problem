package main

import (
	"math/rand"
	"time"
)

func init() {
	// Инициализация пакета rand
	rand.Seed(time.Now().UnixNano())
	//PrintMemUsage("После инициализации пакета math/rand")

	// Инициализация последовательности случаев парковки
	for i := 1; i < CarNumbers; i++ {

		// Генерируем два целых числа в интервале от 0 до HoursInDay
		endTime := uint8(rand.Intn(HoursInDay-1) + 1)
		startTime := uint8(rand.Intn(int(endTime)))

		ParkingTimes = append(ParkingTimes, ParkingTime{
			Start: startTime,
			End:   endTime,
		})
	}
	//PrintMemUsage("После инициализации случаев парковки")
}
