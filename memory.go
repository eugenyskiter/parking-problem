package main

import (
	"fmt"
	"runtime"
)

// PrintMemUsage выводит текущую, общую и используемую память ОС.
// А также количество завершенных циклов сбора "Сборщика мусора".
func PrintMemUsage(msg string) {
	fmt.Println("| --------------------------------------------")
	fmt.Println("| Отчет по использованию памяти: " + msg)

	var m runtime.MemStats
	runtime.ReadMemStats(&m)

	// For info on each, see: https://golang.org/pkg/runtime/#MemStats
	fmt.Printf("| Текущее кол-во выделенной памяти = %v MiB\n", bToMb(m.Alloc))
	fmt.Printf("| Общее кол-во выделенной памяти = %v MiB\n", bToMb(m.TotalAlloc))
	fmt.Printf("| Общий объем памяти, полученной от ОС = %v MiB\n", bToMb(m.Sys))
	fmt.Printf("| Коли-во выполненных 'Сбросов мусора' = %v\n", m.NumGC)

	fmt.Println("| --------------------------------------------")
}

func bToMb(b uint64) uint64 {
	return b / 1024 / 1024
}
