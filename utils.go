package main

import (
	"fmt"
)

func Max(array *[]int) (int, int) {
	var idx = 0
	var max = (*array)[0]

	for k, v := range *array {
		if max < v {
			idx = k
			max = v
		}
	}
	return idx, max
}

func CommonReport(array *[]int) {

	for k, v := range *array {
		fmt.Printf("В %d зафиксированно %d автомобилей\n", k, v)
	}
}

func InBetween(i, min, max *uint8) bool {
	if (*i >= *min) && (*i <= *max) {
		return true
	} else {
		return false
	}
}
